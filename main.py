import datetime
import os

import requests
from multiprocessing.dummy import Pool as ThreadPool
import xlsxwriter


def write_file_to_excell(array):
    userhome = os.path.expanduser('~')
    now = datetime.datetime.now()
    path_to_file = userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(
        now.day) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-Holland America.xlsx'
    if not os.path.exists(userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(
            now.month) + '-' + str(now.day)):
        os.makedirs(
            userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(now.day))
    workbook = xlsxwriter.Workbook(path_to_file)

    worksheet = workbook.add_worksheet()
    worksheet.set_column("A:A", 15)
    worksheet.set_column("B:B", 25)
    worksheet.set_column("C:C", 10)
    worksheet.set_column("D:D", 25)
    worksheet.set_column("E:E", 20)
    worksheet.set_column("F:F", 30)
    worksheet.set_column("G:G", 20)
    worksheet.set_column("H:H", 50)
    worksheet.set_column("I:I", 20)
    worksheet.set_column("J:J", 20)
    worksheet.set_column("K:K", 20)
    worksheet.set_column("L:L", 20)
    worksheet.set_column("M:M", 25)
    worksheet.set_column("N:N", 20)
    worksheet.set_column("O:O", 20)
    worksheet.set_column("P:P", 21)
    worksheet.write('A1', 'DestinationCode')
    worksheet.write('B1', 'DestinationName')
    worksheet.write('C1', 'VesselID')
    worksheet.write('D1', 'VesselName')
    worksheet.write('E1', 'CruiseID')
    worksheet.write('F1', 'CruiseLineName')
    worksheet.write('G1', 'ItineraryID')
    worksheet.write('H1', 'BrochureName')
    worksheet.write('I1', 'NumberOfNights')
    worksheet.write('J1', 'SailDate')
    worksheet.write('K1', 'ReturnDate')
    worksheet.write('L1', 'InteriorBucketPrice')
    worksheet.write('M1', 'OceanViewBucketPrice')
    worksheet.write('N1', 'BalconyBucketPrice')
    worksheet.write('O1', 'SuiteBucketPrice')
    worksheet.write('P1', 'PortList')
    col = 0
    row = 1
    for result in array:
        for item in result:
            if col == {11, 12, 13, 14}:
                try:
                    worksheet.write_number(row, col, item)
                except ValueError:
                    worksheet.write_string(row, col, str(item))
            elif col == {9, 10}:
                worksheet.write(row, col, item)
            else:
                worksheet.write_string(row, col, str(item))
            col += 1
        row += 1
        col = 0
    workbook.close()


def parse(result):
    destination_id = result["regions"]
    itinerary_id = result["itineraryId"]
    destination_name = destination_id
    duration = result["duration"]
    ports = result["portsOfCall"]
    title = result["title"]
    ship_name = result["shipName"]
    ship_id = result["shipId"]
    price_details = \
        session.get("https://www.hollandamerica.com/api/v2/price/itinerary/" + itinerary_id, headers=headers).json()[
            "data"]
    for sailing in price_details:
        interior_price = "N/A"
        oceanview_price = "N/A"
        balcony_price = "N/A"
        suite_price = "N/A"
        departure_date_formatted = datetime.datetime.strptime(sailing["departDate"], '%Y-%m-%d').date()
        arrival_date_formatted = datetime.datetime.strptime(sailing["arriveDate"], '%Y-%m-%d').date()
        cruise_id = sailing["cruiseCode"]
        for room in sailing["roomTypes"]:
            if room["available"]:
                if room["name"] in "Inside":
                    interior_price = room["price"][0]["price"]
                elif room["name"] in "Ocean View":
                    oceanview_price = room["price"][0]["price"]
                elif room["name"] in "Verandah":
                    balcony_price = room["price"][0]["price"]
                elif room["name"] in "Signature Suite":
                    suite_price = room["price"][0]["price"]
            else:
                if room["name"] in "Inside":
                    interior_price = "N/A"
                elif room["name"] in "Ocean View":
                    oceanview_price = "N/A"
                elif room["name"] in "Verandah":
                    balcony_price = "N/A"
                elif room["name"] in "Signature Suite":
                    suite_price = "N/A"
        parsed_results.append(
            [destination_id, destination_name, ship_id, ship_name, cruise_id, "Holland America Cruises",
             itinerary_id, title,
             duration, departure_date_formatted, arrival_date_formatted, interior_price, oceanview_price,
             balcony_price,
             suite_price, ports])


pool = ThreadPool(5)

headers = {
    "Accept": "application/json",
    "Connection": "keep-alive",
    "Referer": "https://www.hollandamerica.com/en_US.html",
    "sec-ch-ua": '"Google Chrome";v="87", " Not;A Brand";v="99", "Chromium";v="87"',
    "content-type": "application/json",
    "country": "US",
    "locale": "en_US",
    "brand": "hal",
    "currencycode": "USD",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"

}

session = requests.session()
parsed_results = []
response = session.get(
    "https://www.hollandamerica.com/search/hal_en_US/select?&fq=(soldOut:(false))&start=0&rows=10000",
    headers=headers)
results = response.json()["searchResults"]
pool.map(parse, results)
pool.close()
pool.join()

write_file_to_excell(parsed_results)

input("Press ENTER key twice to continue...")
